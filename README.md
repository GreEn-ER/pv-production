# PV production

This project aims to model PV production, starting from GreEn-ER own production, to other sites.

## GreEn-ER PV characteristics

### Location : Grenoble, FRANCE
- Latitude : 45.17° North
- Longitude : 5.7°East 
- Time zone = GMT+2
- Altitude = 212m 

## Panels on the roof
### PV Panel :
- DualSun, FLASH 405 Half-Cut: 452 units
- Peak power = 183 kWc
- Efficiency = 15.7%
- Tilt = 6°
- Azimuth 1 = 139° for 220 units
- Azimuth 2 = -41° : for 232 units
### Inverter SMA Sunny Tripower STP50-41 : 5 units

## 2023 APPEEC publication
in folder 2023-APPEEC, you can find an hybrid methodology that couple both data based approach and physic based approach.